<ul class="breadcrumb {{$class}}">
    @foreach($elements as $element)
        @include('breadcrumbs::item',['element' => $element])
    @endforeach
</ul>
