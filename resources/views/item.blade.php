@if($element->getRoute())
    <li class="breadcrumb-item"><a href="{{$element->getRoute()}}">{{$element->getName()}}</a></li>
@else
    <li class="breadcrumb-item"><span>{{$element->getName()}}</span></li>
@endif
