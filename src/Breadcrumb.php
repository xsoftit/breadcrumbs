<?php

namespace Xsoft\Breadcrumbs;

class Breadcrumb
{
    protected $items = [];
    protected $class = '';

    protected function makeBreadcrumb($params)
    {
        foreach ($params as $param) {
            $this->items[] = new BreadcrumbItem($param[0], key_exists(1,$param) ? $param[1] : null);
        }
        return $this;
    }

    public function class($class){
        $this->class = $class;
        return $this;
    }

    public function output()
    {
        return view('breadcrumbs::breadcrumbs', ['elements' => $this->items,'class' => $this->class]);
    }

    public static function __callStatic($method, $arguments)
    {
        if($method = 'make'){
            $breadcrumb = new Breadcrumb();
            return $breadcrumb->makeBreadcrumb($arguments)->output();
        }
    }

}
