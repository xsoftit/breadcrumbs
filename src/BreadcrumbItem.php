<?php

namespace Xsoft\Breadcrumbs;


class BreadcrumbItem
{
    protected $name;

    protected $route;

    public function __construct($name, $route)
    {
        $this->name = $name;
        $this->route = $route;
    }

    public function getRoute(){
        return $this->route;
    }

    public function getName(){
        return $this->name;
    }
}
