<?php

namespace Xsoft\Breadcrumbs;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class BreadcrumbsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'breadcrumbs');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
